﻿30   6    WASHER_BA1024                 Materiau :  / Masse : 2.9 kg              
          -                             -                                         
29   4    VIS_CHC_M3X10                 Materiau :  / Masse : 1 kg                
          - VIS CHC, M 3-10, NF E 27-161-                                         
28   26   VIS_C_M2X16                   Materiau :  / Masse : 0.5 kg              
          - VIS C, M 2-16, NF E 27-115  -                                         
27   6    SCBNJ8_11B                    Materiau : EN_STEEL_INSA / Masse : 18.3 kg
          -                             -                                         
26   3    PSFGW8_50_M5_N5               Materiau : EN_STEEL_INSA / Masse : 16.2 kg
          -                             -                                         
25   4    PIED-RS-20                    Materiau : EN_RUBBER_INSA / Masse : 0 kg  
          - Pied amortissement          - Code commande RS 257-8735               
24   2    MS4_35                        Materiau :  / Masse : 3.3 kg              
          -                             -                                         
23   8    MS1_5_10                      Materiau :  / Masse : 0.1 kg              
          -                             -                                         
22   1    MOTOR-OUT_STD                 Materiau : EN_NYLON_INSA / Masse : 0 kg   
          - CRE0 3.0 COMPONENT          - Designed at INSA Strasbourg             
21   1    MOTOR-OUT_33                  Materiau : EN_NYLON_INSA / Masse : 0 kg   
          - CRE0 3.0 COMPONENT          - Designed at INSA Strasbourg             
20   1    HALF-R3                       Materiau : EN_NYLON_INSA / Masse : 0 kg   
          - CRE0 3.0 COMPONENT          - Designed at INSA Strasbourg             
19   2    HALF-R2                       Materiau : EN_NYLON_INSA / Masse : 0 kg   
          - CRE0 3.0 COMPONENT          - Designed at INSA Strasbourg             
18   3    HALF-R1                       Materiau : EN_NYLON_INSA / Masse : 0 kg   
          - CRE0 3.0 COMPONENT          - Designed at INSA Strasbourg             
17   6    FL688ZZ                       Materiau : EN_STEEL_INSA / Masse : 6.1 kg 
          -                             -                                         
16   9    ENTRETOISE-RS806-6654         Materiau :  / Masse : 16.7 kg             
          -                             -                                         
15   4    ENTRETOISE-RS806-6651         Materiau :  / Masse : 20.6 kg             
          -                             - Entretoise RS                           
14   4    ECRK18                        Materiau :  / Masse : 15.8 kg             
          -                             -                                         
13   6    DEI4762_M3X10                 Materiau : EN_STEEL_INSA / Masse : 1 kg   
          -                             -                                         
12   3    CORE_BA1024                   Materiau :  / Masse : 5.5 kg              
          -                             -                                         
11   5    BOX_SFB5_15                   Materiau :  / Masse : 2.9 kg              
          -                             -                                         
10   3    BCHLJJ5_MB                    Materiau : EN_STEEL_INSA / Masse : 2.7 kg 
          -                             -                                         
9    3    BCHLJJ5_B                     Materiau : EN_NYLON_INSA / Masse : 0 kg   
          -                             -                                         
8    1    BASE2                         Materiau : EN_ALU_INSA / Masse : 2.9 kg   
          - CRE0 3.0 COMPONENT          - Bati - Plaque supérieure                
7    1    BASE1                         Materiau : EN_ALU_INSA / Masse : 2.9 kg   
          - CRE0 3.0 COMPONENT          - Bati - Plaque inférieure                
6    1    BAR4                          Materiau : EN_ALU_INSA / Masse : 0 kg     
          - CRE0 3.0 COMPONENT          - Designed at INSA Strasbourg             
5    1    BAR3                          Materiau : EN_ALU_INSA / Masse : 0 kg     
          - CRE0 3.0 COMPONENT          - Designed at INSA Strasbourg             
4    1    BAR2                          Materiau : EN_ALU_INSA / Masse : 0 kg     
          - CRE0 3.0 COMPONENT          - Designed at INSA Strasbourg             
3    1    BAR1                          Materiau : EN_ALU_INSA / Masse : 0 kg     
          - CRE0 3.0 COMPONENT          - Designed at INSA Strasbourg             
2    2    AX-12A_SW2                    Materiau : UNASSIGNED / Masse : 0 kg      
          - CRE0 3.0 COMPONENT          - Designed at INSA Strasbourg             
1    2    AX-12A_SW1                    Materiau : UNASSIGNED / Masse : 0.3 kg    
          - CRE0 3.0 COMPONENT          - Designed at INSA Strasbourg             
Rep. Qté. NOM fichier                   MATERIAU / MASSE                          
          - DESIGNATION composant       DESCRIPTION_DETAILLEE composant           
