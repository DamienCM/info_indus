import main
import numpy as np
from sympy import I
import matplotlib.pyplot as plt


def compute_grid(rob, xx, yy):
    reachable = np.zeros_like(xx).astype(np.bool8)
    last = 0
    for ix, iy in np.ndindex(xx.shape):
        if ix != last:
            print(ix)
        try:
            q1, q2 = rob.MGI(xx[ix, iy], yy[ix, iy])
            reachable[ix, iy] = True
        except ValueError:
            reachable[ix, iy] = False
        last = ix
    return reachable


def plot_reachable(x, y, reachable):
    fig, ax = plt.gcf(), plt.gca()
    ax.set_xlabel("x [mm]")
    ax.set_ylabel("y [mm]")
    ax.imshow(reachable, origin='lower', cmap="gray_r",interpolation='none', extent=[-100,250,0,225])



if __name__ == '__main__':
    rob = main.robot()
    fig, ax = plt.subplots(figsize=(16/1.5, 9/1.5))
    fig.tight_layout()
    N = 100
    x = np.linspace(-100, 250, N)
    y = np.linspace(-0, 225, N)
    xx, yy = np.meshgrid(x, y)

    reachable = compute_grid(rob, xx, yy)
    print("computed")
    plot_reachable(x, y, reachable)
    plt.imsave("reachable.tiff", reachable, cmap="gray_r")
    # plt.savefig('espace_travail.tif',dpi=600)
    plt.show()
    
