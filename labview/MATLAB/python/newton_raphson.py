import numpy as np
from numpy.linalg import norm, inv


def newton_raphson(f: callable,  x0: np.array, jacobian: callable, tol = 1e-6, max_iter = 1e3, printing=False) -> np.array:
    """
    Newton-Raphson method for finding the roots of a function
    :param f: function to find the root of
    :param x0: initial guess
    :param tol: tolerance
    :param max_iter: maximum number of iterations
    :return: root of the function
    """
    x = x0
    it = 0
    fx = f(x)
    if type(x) != np.ndarray:
        raise TypeError("x0 must be a numpy array")
    if type(fx) != np.ndarray:
        raise TypeError("f(x) must be a numpy array")
    if norm(fx) < tol:
        raise ValueError("f(x0) is already close enough to zero")

    
    
    while norm(fx) > tol and it < max_iter:
        it += 1
        J = jacobian(x)
        x = x - np.dot(inv(J), fx)
        fx = f(x)
        it += 1

    if printing:
        print("Number of iterations: ", it)
        print("Error: ", norm(fx))
        print("Root: ", x)
    
    if it == max_iter:
        raise ValueError("Maximum number of iterations reached")
        print("Maximum number of iterations reached")

    return x
