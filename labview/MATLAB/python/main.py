# %% [markdown]
# # Maquette 5 barres

# %%
from os import error
import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import norm, inv
from scipy.optimize import minimize, optimize, newton
import timeit
from newton_raphson import newton_raphson
import pickle
import sympy as sp
from sympy import pi, sin, cos, I
import time
from PIL import Image


class robot():
    def __init__(self):
        """
        Initialize the robot
        """
        # %%
        # Definition des constantes vue de dessus
        # R : 1 a 5 en mm
        #r1, r2, r3, r4, r5

        self.posList = []

        # Segments lengths
        self.R = [80, 79, 157, 147, 138]

        # Symbolics
        self.r1, self.r2, self.r3, self.r4, self.r5 = sp.symbols(
            'r_1 r_2 r_3 r_4 r_5')
        self.r = [self.r1, self.r2, self.r3, self.r4, self.r5]

        # Same for qis
        self.q1, self.q2, self.q3, self.q4, self.q5 = sp.symbols(
            'q_1 q_2 q_3 q_4 q_5')
        self.q = [self.q1, self.q2, self.q3, self.q4, self.q5]

        # define xp,yp
        self.xp, self.yp = sp.symbols('x_p y_p')

        # %%
        # Define the vector
        self.A3A1 = [
            -self.xp,
            -self.yp,
        ]

        self.A1A2 = [
            self.r1*cos(self.q1),
            self.r1*sin(self.q1)
        ]

        A2A3 = [
            self.r3*cos(self.q3),
            self.r3*sin(self.q3)
        ]

        A4A3 = [
            +self.r4*cos(self.q4),
            +self.r4*sin(self.q4)
        ]

        A5A4 = [
            +self.r2*cos(self.q2),
            +self.r2*sin(self.q2)
        ]

        self.A1A5 = [
            self.r5,
            0
        ]

        # # %%
        # # Boucle 1
        # vectors_b1_r = [A1A2[0]+A3A1[0], A1A2[1]+A3A1[1]]
        # vectors_b1_l = [-A2A3[0], -A2A3[1]]
        # # Add squares
        # eq1_l_sq = vectors_b1_l[0]**2 + vectors_b1_l[1]**2
        # eq1_r_sq = vectors_b1_r[0]**2 + vectors_b1_r[1]**2
        # # Solution
        # print(sp.trigsimp(sp.expand(eq1_r_sq)), " = ", sp.trigsimp(eq1_l_sq))
        # sols_q1 = sp.solve(sp.Eq(sp.trigsimp(eq1_l_sq),
        #                    sp.trigsimp(sp.expand(eq1_r_sq))), q1)

        # # Boucle 2
        # vectors_b2_r = [A1A5[0] + A5A4[0] + A3A1[0], A1A5[1] + A5A4[1] + A3A1[1]]
        # vectors_b2_l = [-A4A3[0], -A4A3[1]]
        # # Square
        # eq2_l_sq = vectors_b2_l[0]**2 + vectors_b2_l[1]**2
        # eq2_r_sq = vectors_b2_r[0]**2 + vectors_b2_r[1]**2
        # # Solution
        # print(sp.trigsimp(sp.expand(eq2_r_sq)), " = ", sp.trigsimp(eq2_l_sq))
        # sols_q2 = sp.solve(sp.Eq(sp.trigsimp(eq2_l_sq),
        #                    sp.trigsimp(sp.expand(eq2_r_sq))), q2)

        # # save to file
        # with open('sols_q1.eq', 'wb') as f:
        #     pickle.dump(sols_q1,f)

        # with open('sols_q2.eq', 'wb') as f:
        #     pickle.dump(sols_q2,f)

        # %%
        # with open('sols_q1.eq', 'rb') as f:
        #     self.sols_q1 = pickle.load(f)

        # with open('sols_q2.eq', 'rb') as f:
        #     self.sols_q2 = pickle.load(f)


        # self.sols_q1 = self.sols_q1[1].subs(values)
        # self.sols_q2 =  self.sols_q2[0].subs(values)

    
    # def sols_q1(self,xp,yp):
    #     r1 =  self.R[0]
    #     r2 =  self.R[1]
    #     r3 =  self.R[2]
    #     r4 =  self.R[3]
    #     r5 =  self.R[4]
    #     q1 = 2*np.arctan((2*r1*yp+np.sqrt((-r1**2+2*r1*r3+r3**2+xp**2+yp**2)*(r1**2+2*r1*r3+r3**2-xp**2-yp**2)))/(r1**2+2*r1*xp-r3**2+xp**2+yp**2))
    #     q2 = 2*np.arctan((2*r2*yp-np.sqrt((-r2**2+2*r2*r4+r4**2+r5**2-2*r5*xp+xp**2+yp**2)*(r2**2+2*r2*r4+r4**2-r5**2+2*r5*xp-xp**2-yp**2)))/(r2**2-2*r2*r5+2*r2*xp-r4**2+r5**2-2*r5*xp+xp**2+yp**2))

    # %%

    def MGI(self, x=None, y=None):
        """
        Compute the joint angles for a given x,y pose

        Args:
            x ([float]): [xpose] if None, uses self.xpose
            y ([float]): [ypose] if None, uses self.ypose

        Returns:
            [q1(pose1), q1(pose2)], [q2(pose1), q2(pose2)] 
        """
        if x is None and y is None:
            x = self.xp
            y = self.yp
        # values = {
        #     "x_p": x,
        #     "y_p": y
        # }
        # t0 = time.perf_counter()
        # self.q1 = self.sols_q1.subs(values)
        # t1 = time.perf_counter()
        # self.q1 = self.q1.evalf() % (2*np.pi)
        # t2 = time.perf_counter()
        # print(f'time 1 : {t1-t0}, time2 = {t2-t1}')
        # self.q2 = self.sols_q2.subs(values).evalf() % (2*np.pi)
        # return self.q1, self.q2
        r1 =  self.R[0]
        r2 =  self.R[1]
        r3 =  self.R[2]
        r4 =  self.R[3]
        r5 =  self.R[4]
        xp,yp =x,y
        
        if ((-r1**2+2*r1*r3-r3**2+xp**2+yp**2)*(r1**2+2*r1*r3+r3**2-xp**2-yp**2)) < 0 or ((-r2**2+2*r2*r4-r4**2+r5**2-2*r5*xp+xp**2+yp**2)*(r2**2+2*r2*r4+r4**2-r5**2+2*r5*xp-xp**2-yp**2)) < 0 :
            raise(ValueError("Position innateignable"))

        q1 = 2*np.arctan((2*r1*yp+np.sqrt((-r1**2+2*r1*r3-r3**2+xp**2+yp**2)*(r1**2+2*r1*r3+r3**2-xp**2-yp**2)))/(r1**2+2*r1*xp-r3**2+xp**2+yp**2))%(2*np.pi)
        
        q2 = 2*np.arctan((2*r2*yp-np.sqrt((-r2**2+2*r2*r4-r4**2+r5**2-2*r5*xp+xp**2+yp**2)*(r2**2+2*r2*r4+r4**2-r5**2+2*r5*xp-xp**2-yp**2)))/(r2**2-2*r2*r5+2*r2*xp-r4**2+r5**2-2*r5*xp+xp**2+yp**2))%(2*np.pi)

        self.q1 = q1
        self.q2 = q2

        return q1, q2
    def compute_points(self, q1, q2):
        """Compute the point list for a given position of motors

        Args:
            q1 (float): angle
            q2 (float): angle

        Returns:
            list: positions angles 

        """
        xa, ya = 0, 0
        xa2, ya2 = self.R[0] * cos(q1), self.R[0]*sin(q1)
        xa5, ya5 = self.R[-1], 0
        xa4, ya4 = self.R[-1] + self.R[1] * cos(q2), self.R[1]*sin(q2)

        return [xa, ya, xa2, ya2, xa5, ya5, xa4, ya4]

    def initialize_fig(self):
        """
        create the robot in an interactive figure and set the robot to a default position
        """
        self.fig, self.ax = plt.subplots(figsize=(16*0.8, 9*0.8))
        self.ax.set_ylim([-50, 300])
        self.ax.set_xlim([-100, 250])
        self.ax.grid(True)

        self.xp, self.yp = 70, 100
        self.q1, self.q2 = self.MGI()
        self.xa, self.ya, self.xa2, self.ya2, self.xa5, self.ya5, self.xa4, self.ya4 = self.compute_points(
            self.q1, self.q2)
        self.xa, self.ya = 0, 0
        self.xa2, self.ya2 = self.R[0] * cos(self.q1), self.R[0]*sin(self.q1)
        self.xa5, self.ya5 = self.R[-1], 0
        self.xa4, self.ya4 = self.R[-1] + self.R[1] * \
            cos(self.q2), self.R[1]*sin(self.q2)

        self.leg, = self.ax.plot([self.xa, self.xa2, self.xp, self.xa4, self.xa5], [self.ya, self.ya2, self.yp, self.ya4, self.ya5],
                                 color='r', marker='o', mfc='r', mec='r', lw=8, ms=15)
        self.title = self.fig.suptitle("Position atteignable", color='green')

    def set_robot_positions(self, values):
        """Set the postions of each joint in xy

        Args:
            values (list): float
        """
        if values is not None:
            self.xa = values[0]
            self.ya = values[1]
            self.xa2 = values[2]
            self.ya2 = values[3]
            self.xa5 = values[4]
            self.ya5 = values[5]
            self.xa4 = values[6]
            self.ya4 = values[7]

        self.leg.set_data([self.xa, self.xa2, self.xp, self.xa4, self.xa5], [
                          self.ya, self.ya2, self.yp, self.ya4, self.ya5])

    def show(self):
        # plt.connect('button_press_event', self.leg_update)
        plt.connect('motion_notify_event', self.leg_update)
        plt.show()

    def plot_legs(self, q1, q2, q3, q4, x, y):
        """
        Plot the legs of the effector given all parameters

        Args:
            q1 (float): angle rad
            q2 (float): angle rad
            q3 (float): angle rad
            q4 (float): angle rad
            x (float): mm
            y (float): mm

        """

        r1 = self.R[0]
        r2 = self.R[1]
        r3 = self.R[2]
        r4 = self.R[3]
        r5 = self.R[4]

        s1 = np.sin(q1)
        c1 = np.cos(q1)

        s2 = np.sin(q2)
        c2 = np.cos(q2)

        s3 = np.sin(q3)
        c3 = np.cos(q3)

        s4 = np.sin(q4)
        c4 = np.cos(q4)

        x1, y1 = 0, 0
        x2, y2 = r1*c1, r1*s1
        x3a, y3a = x2+r3*c3, y2+r3*s3
        x5, y5 = r5, 0
        x4, y4 = x5+r2*c2, y5+r2*s2
        x3b, y3b = x4 + r4*c4, y4 + r4*s4

        self.leg.set_data([x1, x2, x3a, x3b, x4, x5], [y1, y2, y3a, y3b, y4, y5])
        # self.leg.set_data([x5, x4, x3b], [y5, y4, y3b])
        # plt.scatter(x, y, color='k')
        # plt.axis('equal')



    def leg_update(self, event):
        """update the leg pose when a click occurs

        Args:
            event (event): event comming from mpl figure

        """
        if event.button is None :
            return
        xt, yt = event.xdata, event.ydata
        # print(f"Event : {event.button}")
    
        if event.button == 2 :
            # print("Click gauche")
            try :
                q1, q2 = self.MGI(xt, yt)
                xa, ya, xa2, ya2, xa5, ya5, xa4, ya4 = self.compute_points(
                q1, q2)
                self.fig.suptitle("Position atteignable", color='g')
                self.leg.set_data([xa, xa2, xt, xa4, xa5], [ya, ya2, yt, ya4, ya5])
            except ValueError:
                self.fig.suptitle("Position non atteignable", color='r')
            self.fig.canvas.draw()
        elif event.button == 1:
            # print("Click droit")
            try:
                self.q1 = np.arctan2(yt, xt)
                # print(f"angle = {self.q1*180/np.pi} deg")
                self.q3, self.q4, self.x, self.y = self.MGD(self.q1,self.q2)
                # print(f"new values = {self.q3*180/np.pi}, {self.q4*180/np.pi}, {self.x}, {self.y}")
                self.plot_legs(self.q1, self.q2, self.q3, self.q4, self.x, self.y)
                self.fig.suptitle("Position atteignable", color='g')
                # print("set position")

            except ValueError:
                self.fig.suptitle("Position non atteignable", color='r')
        elif event.button == 3:
            # print("Clic molette")
            try:
                self.q2 = np.arctan2(yt, xt-self.R[-1])
                self.q3, self.q4, self.x, self.y = self.MGD(self.q1,self.q2)
                self.plot_legs(self.q1, self.q2, self.q3, self.q4, self.x, self.y)
                self.fig.suptitle("Position atteignable", color='g')

            except ValueError:
                self.fig.suptitle("Position non atteignable", color='r')
        else :
            return
        self.fig.canvas.draw()

    def error(self, vect):

        q3 = vect[0]
        q4 = vect[1]
        x = vect[2]
        y = vect[3]

        r1 = self.R[0]
        r2 = self.R[1]
        r3 = self.R[2]
        r4 = self.R[3]
        r5 = self.R[4]

        s1 = np.sin(self.q1)
        c1 = np.cos(self.q1)

        s2 = np.sin(self.q2)
        c2 = np.cos(self.q2)

        s3 = np.sin(q3)
        c3 = np.cos(q3)

        s4 = np.sin(q4)
        c4 = np.cos(q4)

        eqa = r5+r2*c2+r4*c4-x
        eqb = r2*s2+r4*s4-y
        eqc = r1*c1+r3*c3-x
        eqd = r1*s1+r3*s3-y
        
        # J = [
        #     [0, -r4*s4, -1, 0], # a
        #     [0, r4*c4, 0, -1], # b
        #     [-r3*c3, 0, -1, 0], # c
        #     [r3*c3, 0, 0, -1] # d
        # ] # q3 q4 x y  

        return np.array([eqa, eqb, eqc, eqd])

    def jacobian(self, vect):


        q3 = vect[0]
        q4 = vect[1]
        x = vect[2]
        y = vect[3]

        r1 = self.R[0]
        r2 = self.R[1]
        r3 = self.R[2]
        r4 = self.R[3]
        r5 = self.R[4]

        s1 = np.sin(self.q1)
        c1 = np.cos(self.q1)

        s2 = np.sin(self.q2)
        c2 = np.cos(self.q2)

        s3 = np.sin(q3)
        c3 = np.cos(q3)

        s4 = np.sin(q4)
        c4 = np.cos(q4)

        # eqa = r5+r2*c2+r4*c4-x
        # eqb = r2*s2+r4*s4-y
        # eqc = r1*c1+r3*c3-x
        # eqd = r1*s1+r3*s3-y
        
        J = np.array([
            [0, -r4*s4, -1, 0], # a
            [0, r4*c4, 0, -1], # b
            [-r3*c3, 0, -1, 0], # c
            [r3*c3, 0, 0, -1] # d
        ]) # q3 q4 x y  

        return J


    def MGD(self, q1, q2, x0 = None):
        if x0 is None:
            x0 =  np.array([self.q3, self.q4, self.x, self.y]).transpose() 
        
        # eq 1 : A1A2 A2A3 = P
        # eq 2 : A5A4 A4A3 = P
        # Projection 4 eq, 4 inconnues (q3, q4, x, y)

        self.q1 = q1
        self.q2 = q2      
        # x0 = np.array([np.pi/6, np.pi*2/3, 70, 100]).transpose()
        # x0 = np.array([self.q3, self.q4, self.x, self.y]).transpose()
        x = newton_raphson(self.error, x0, self.jacobian)
        # q3 q4 x y
        self.q3, self.q4, self.x, self.y = x.transpose()
        return x


if __name__ == "__main__":
    rob = robot()
    q1, q2 = 0.8*2*np.pi/3, 1.2*np.pi/3
    x0 = np.array([np.pi/6, np.pi*2/3, 70, 100]).transpose()
    q3, q4, x, y = rob.MGD(q1, q2, x0)
    rob.initialize_fig()
    reachable = plt.imread("reachable.tiff")
    plt.imshow(reachable, origin='lower', cmap="gray_r",interpolation='none', extent=[-100,250,0,225])

    # rob.plot_legs(q1, q2, q3, q4, x, y)
    rob.show()
    # text= ax.text(-100, 175, "Position atteignable",color ='green')
